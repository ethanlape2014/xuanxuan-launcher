package org.ethan.launcher.settings;

import android.os.Bundle;
import android.preference.PreferenceFragment;

import org.ethan.launcher.R;

/**
 * Created by ethan on 2/18/16.
 */
public class SettingsFragment extends PreferenceFragment {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Load the preferences from an XML resource
        addPreferencesFromResource(R.xml.preferences);
    }
}
