package org.ethan.launcher.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.GridView;

import org.ethan.launcher.adapter.AppHomeAdapter;
import org.ethan.launcher.R;
import org.ethan.launcher.app.AppDetail;

import java.util.ArrayList;

public class HomeActivity extends Activity {
    public static ArrayList<AppDetail> homeApps = new ArrayList<>();

    private static final int NO_OPEN_POS = -1;

    private static int openPos;
    private static AppDetail blankAppSpace;

    private SharedPreferences pref;

    private PackageManager manager;
    private GridView appsGrid;
    private AppHomeAdapter adapter;

    private boolean isInFront;
    private boolean inEditMode;
    private boolean needGridResize;

    private int selectedAppPos;
    private int screenHeight;
    private int appColumns;
    private int appRows;

    private int appIconSize;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        pref = PreferenceManager.getDefaultSharedPreferences(this);

        SharedPreferences.OnSharedPreferenceChangeListener listener = new SharedPreferences.OnSharedPreferenceChangeListener() {
            @Override
            public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
                loadAppSettings();
            }
        };

        inEditMode = false;
        selectedAppPos = NO_OPEN_POS;
        needGridResize = true;

        pref.registerOnSharedPreferenceChangeListener(listener);

        manager = getPackageManager();
        appsGrid = (GridView) findViewById(R.id.home_apps_list);
        adapter = new AppHomeAdapter(this, homeApps);

        detectScreenSize();

        blankAppSpace = new AppDetail(null, null, null, true);
        blankAppSpace.icon = ContextCompat.getDrawable(getApplicationContext(), R.mipmap.ic_launcher);

        addSettings();
        loadAppSettings();
        resizeAppsGrid();
        reloadApps();
        addClickListener();
    }

    private void addSettings() {
        SharedPreferences.Editor edit = pref.edit();

        Resources res = getResources();

        if (!pref.contains("app_rows")) {
            edit.putString("app_rows", getString(R.string.default_rows));
        }

        if (!pref.contains("app_columns")) {
            edit.putString("app_columns", getString(R.string.default_columns));
        }

        if (!pref.contains("show_home_app_label")) {
            edit.putBoolean("show_home_app_label", res.getBoolean(R.bool.default_showHomeAppLabel));
        }

        edit.apply();
    }

    private void loadAppSettings() {
        Resources res = getResources();

        appRows = Integer.parseInt(pref.getString("app_rows", getString(R.string.default_rows)));
        appColumns = Integer.parseInt(pref.getString("app_columns", getString(R.string.default_columns)));

        if (adapter != null) {
            adapter.showAppLabel = pref.getBoolean("show_home_app_label", res.getBoolean(R.bool.default_showHomeAppLabel));
        }

        needGridResize = true;
    }

    private void detectScreenSize() {
        DisplayMetrics d = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(d);

        screenHeight = d.heightPixels;

        Resources res = getResources();

        int id = res.getIdentifier("navigation_bar_height", "dimen", "android");

        if (id > 0) {
            screenHeight -= res.getDimensionPixelOffset(id);
        }

        id = res.getIdentifier("status_bar_height", "dimen", "android");

        if (id > 0) {
            screenHeight -= res.getDimensionPixelOffset(id);
        }

        id = res.getIdentifier("app_icon_size", "dimen", "android");

        if (id > 0) {
            appIconSize = res.getDimensionPixelSize(id);
        }
    }

    private void resizeAppsGrid() {
        int spacing = screenHeight - (appRows * appIconSize);

        spacing /= appRows;

        appsGrid.setNumColumns(appColumns);
        appsGrid.setVerticalSpacing(spacing);

        ArrayList<AppDetail> temp = new ArrayList<>();

        int appCount = appColumns * appRows;

        for (int i = 0; i < appCount; i++) {
            temp.add(AppDetail.EMPTY_APP);
        }

        for (int i = homeApps.size() - 1, j = appCount - 1; i >= 0 && j >= 0; i--, j--) {
            temp.set(j, homeApps.get(i));
        }

        homeApps = temp;

        findOpenPos();

        needGridResize = false;
    }

    private void reloadApps() {
        adapter.setData(homeApps);

        appsGrid.setAdapter(adapter);
    }

    @Override
    public void onStart() {
        super.onStart();

        isInFront = true;

        if (needGridResize) {
            resizeAppsGrid();
        }

        reloadApps();
        exitEditMode();
    }

    @Override
    public void onStop() {
        super.onStop();

        isInFront = false;
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        if (inEditMode) {
            exitEditMode();
        } else if (isInFront) {
            showApps(this.getCurrentFocus());
        }
    }

    public static boolean addApp(AppDetail app) {
        if (openPos == NO_OPEN_POS) {
            return false;
        }

        homeApps.set(openPos, app);

        findOpenPos();

        return true;
    }

    private void removeApp(int pos) {
        if (pos == NO_OPEN_POS) {
            return;
        }

        if (!inEditMode) {
            return;
        }

        homeApps.set(pos, blankAppSpace);
    }

    private static void findOpenPos() {
        for (int i = homeApps.size() - 1; i >= 0; i--) {
            if (homeApps.get(i).empty) {
                openPos = i;
                return;
            }
        }

        openPos = NO_OPEN_POS;
    }

    private void enterEditMode() {
        inEditMode = true;

        for (int i = 0; i < homeApps.size(); i++) {
            if (homeApps.get(i).empty) {
                homeApps.set(i, blankAppSpace);
            }
        }

        reloadApps();
    }

    private void exitEditMode() {
        inEditMode = false;
        selectedAppPos = NO_OPEN_POS;

        for (int i = 0; i < homeApps.size(); i++) {
            if (homeApps.get(i).empty) {
                homeApps.set(i, AppDetail.EMPTY_APP);
            }
        }

        reloadApps();
        findOpenPos();
    }

    private void selectApp(int pos) {
        selectedAppPos = pos;
    }

    private void launchApp(View v, int pos) {
        AppDetail app = adapter.getItem(pos);

        if (app.empty) {
            return;
        }

        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
        Intent i = manager.getLaunchIntentForPackage(app.name.toString());
        HomeActivity.this.startActivity(i);
    }

    private void swapSelectedApp(int pos) {
        AppDetail app = homeApps.get(pos);

        homeApps.set(pos, homeApps.get(selectedAppPos));
        homeApps.set(selectedAppPos, app);

        reloadApps();
        findOpenPos();

        selectedAppPos = NO_OPEN_POS;
    }

    private void addClickListener() {
        appsGrid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> av, View v, int pos, long id) {
                if (inEditMode) {
                    if (selectedAppPos == NO_OPEN_POS) {
                        selectApp(pos);
                    } else {
                        swapSelectedApp(pos);
                    }
                } else {
                    launchApp(v, pos);
                }
            }
        });

        appsGrid.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            public boolean onItemLongClick(AdapterView<?> av, View v, int pos, long id) {
                if (inEditMode) {
                    removeApp(pos);
                } else {
                    enterEditMode();
                }

                return true;
            }
        });
    }

    public void showApps(View view) {
        Intent i = new Intent(this, AppListActivity.class);
        startActivity(i);
    }
}
