package org.ethan.launcher.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import org.ethan.launcher.R;
import org.ethan.launcher.app.AppDetail;
import org.ethan.launcher.adapter.AppSearchAdapter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class AppListActivity extends Activity {
    private PackageManager manager;
    private ListView list;
    private AppSearchAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_apps_list);

        loadApps();
        addClickListener();
        addSearchListener();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return false;
    }

    public void OnClickSettingsButton(View view) {
        startActivity(new Intent(this, SettingsActivity.class));
    }

    private void loadApps() {
        manager = getPackageManager();
        ArrayList<AppDetail> apps = new ArrayList<>();

        Intent i = new Intent(Intent.ACTION_MAIN, null);
        i.addCategory(Intent.CATEGORY_LAUNCHER);

        List<ResolveInfo> available = manager.queryIntentActivities(i, 0);

        for (ResolveInfo info : available) {
            AppDetail app = new AppDetail();
            app.label = info.loadLabel(manager);
            app.name = info.activityInfo.packageName;
            app.icon = info.activityInfo.loadIcon(manager);
            app.empty = false;

            apps.add(app);
        }

        Collections.sort(apps, new Comparator<AppDetail>() {
            public int compare(AppDetail l, AppDetail r) {
                return l.label.toString().compareTo(r.label.toString());
            }
        });

        adapter = new AppSearchAdapter(this, apps);

        list = (ListView) findViewById(R.id.app_list);
        list.setAdapter(adapter);

        list.requestFocus();
    }

    private void addClickListener() {
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> av, View v, int pos, long id) {
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                Intent i = manager.getLaunchIntentForPackage(adapter.getItem(pos).name.toString());
                AppListActivity.this.startActivity(i);
            }
        });

        list.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            public boolean onItemLongClick(AdapterView<?> av, View v, int pos, long id) {
                AppDetail app = adapter.getItem(pos);

                CharSequence text = "Added " + app.label + " to home";

                if (!HomeActivity.addApp(app)) {
                    text = "Home screen full";
                }

                Context context = getApplicationContext();

                int duration = Toast.LENGTH_SHORT;

                Toast toast = Toast.makeText(context, text, duration);
                toast.show();

                return true;
            }
        });
    }

    private void addSearchListener() {
        EditText search = (EditText) findViewById(R.id.search_bar);

        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                adapter.filter(s.toString());
            }
        });
    }
}
