package org.ethan.launcher.app;

import android.graphics.drawable.Drawable;
import android.widget.Filter;
import android.widget.Filterable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ethan on 2/9/16.
 */
public class AppDetail {
    public static final AppDetail EMPTY_APP = new AppDetail(null, null, null, true);

    public CharSequence label;
    public CharSequence name;
    public Drawable icon;
    public boolean empty;

    public AppDetail() {
        this(null, null, null, true);
    }

    public AppDetail(CharSequence label, CharSequence name, Drawable icon, boolean empty) {
        this.label = label;
        this.name = name;
        this.icon = icon;
        this.empty = empty;
    }
}
