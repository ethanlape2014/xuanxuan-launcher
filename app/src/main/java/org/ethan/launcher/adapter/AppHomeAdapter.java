package org.ethan.launcher.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import org.ethan.launcher.R;
import org.ethan.launcher.app.AppDetail;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class AppHomeAdapter extends BaseAdapter {
    private LayoutInflater inflater;

    private ArrayList<AppDetail> apps;

    public boolean showAppLabel;

    public AppHomeAdapter(Context context, ArrayList<AppDetail> apps) {
        this.apps = new ArrayList<>(apps);

        inflater = LayoutInflater.from(context);
    }

    public void setData(ArrayList<AppDetail> apps) {
        this.apps = apps;
    }

    public int getCount() {
        return apps.size();
    }

    public long getItemId(int position) {
        return position;
    }

    public AppDetail getItem(int position) {
        return apps.get(position);
    }

    public View getView(final int position, View view, ViewGroup parent) {
        if (view == null) {
            view = inflater.inflate(R.layout.home_icon, parent, false);
        }

        ImageView appIcon = (ImageView) view.findViewById(R.id.home_app_icon);
        appIcon.setImageDrawable(apps.get(position).icon);
        appIcon.setMinimumHeight(96);

        TextView appLabel = (TextView) view.findViewById(R.id.home_app_label);

        if (showAppLabel) {
            appLabel.setText(apps.get(position).label);
        } else {
            appLabel.setVisibility(TextView.INVISIBLE);
            appLabel.setHeight(0);
        }

        return view;
    }
}
