package org.ethan.launcher.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import org.ethan.launcher.R;
import org.ethan.launcher.app.AppDetail;

import java.util.ArrayList;
import java.util.Locale;

public class AppSearchAdapter extends BaseAdapter {
    private LayoutInflater inflater;

    private ArrayList<AppDetail> apps;
    private ArrayList<AppDetail> filtered;

    public AppSearchAdapter(Context context, ArrayList<AppDetail> apps) {
        this.apps = new ArrayList<>(apps);
        this.filtered = new ArrayList<>(apps);

        inflater = LayoutInflater.from(context);
    }

    public int getCount() {
        return filtered.size();
    }

    public long getItemId(int position) {
        return position;
    }

    public AppDetail getItem(int position) {
        return filtered.get(position);
    }

    public View getView(final int position, View view, ViewGroup parent) {
        if (view == null) {
            view = inflater.inflate(R.layout.list_item, parent, false);
        }

        ImageView appIcon = (ImageView) view.findViewById(R.id.item_app_icon);
        appIcon.setImageDrawable(filtered.get(position).icon);

        TextView appLabel = (TextView) view.findViewById(R.id.item_app_label);
        appLabel.setText(filtered.get(position).label);

        TextView appName = (TextView) view.findViewById(R.id.item_app_name);
        appName.setText(filtered.get(position).name);

        return view;
    }

    public void filter(String str) {
        str = str.trim().toLowerCase(Locale.getDefault());

        filtered.clear();

        if (str.length() == 0) {
            filtered.addAll(apps);
        } else {
            for (AppDetail app : apps) {
                if (app.label.toString().toLowerCase(Locale.getDefault()).contains(str)) {
                    filtered.add(app);
                }
            }
        }

        notifyDataSetChanged();
    }
}
